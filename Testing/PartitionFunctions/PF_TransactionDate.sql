CREATE PARTITION FUNCTION [PF_TransactionDate] ([date])
	AS RANGE RIGHT
	FOR VALUES ('20140101 00:00:00.000', '20150101 00:00:00.000', '20160101 00:00:00.000', '20170101 00:00:00.000')
GO
