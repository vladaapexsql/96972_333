SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Sales].[CustomerTransactions] (
		[CustomerTransactionID]     [int] NOT NULL,
		[CustomerID]                [int] NOT NULL,
		[TransactionTypeID]         [int] NOT NULL,
		[InvoiceID]                 [int] NULL,
		[PaymentMethodID]           [int] NULL,
		[TransactionDate]           [date] NOT NULL,
		[AmountExcludingTax]        [decimal](18, 2) NOT NULL,
		[TaxAmount]                 [decimal](18, 2) NOT NULL,
		[TransactionAmount]         [decimal](18, 2) NOT NULL,
		[OutstandingBalance]        [decimal](18, 2) NOT NULL,
		[FinalizationDate]          [date] NULL,
		[IsFinalized]               AS (case when [FinalizationDate] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end) PERSISTED,
		[LastEditedBy]              [int] NOT NULL,
		[LastEditedWhen]            [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Sales_CustomerTransactions]
		PRIMARY KEY
		NONCLUSTERED
		([CustomerTransactionID])
	ON [USERDATA]
)
GO
ALTER TABLE [Sales].[CustomerTransactions]
	ADD
	CONSTRAINT [DF_Sales_CustomerTransactions_CustomerTransactionID]
	DEFAULT (NEXT VALUE FOR [Sequences].[TransactionID]) FOR [CustomerTransactionID]
GO
ALTER TABLE [Sales].[CustomerTransactions]
	ADD
	CONSTRAINT [DF_Sales_CustomerTransactions_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Sales].[CustomerTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_CustomerTransactions_CustomerID_Sales_Customers]
	FOREIGN KEY ([CustomerID]) REFERENCES [Sales].[Customers] ([CustomerID])
ALTER TABLE [Sales].[CustomerTransactions]
	CHECK CONSTRAINT [FK_Sales_CustomerTransactions_CustomerID_Sales_Customers]

GO
ALTER TABLE [Sales].[CustomerTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_CustomerTransactions_TransactionTypeID_Application_TransactionTypes]
	FOREIGN KEY ([TransactionTypeID]) REFERENCES [Application].[TransactionTypes] ([TransactionTypeID])
ALTER TABLE [Sales].[CustomerTransactions]
	CHECK CONSTRAINT [FK_Sales_CustomerTransactions_TransactionTypeID_Application_TransactionTypes]

GO
ALTER TABLE [Sales].[CustomerTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_CustomerTransactions_InvoiceID_Sales_Invoices]
	FOREIGN KEY ([InvoiceID]) REFERENCES [Sales].[Invoices] ([InvoiceID])
ALTER TABLE [Sales].[CustomerTransactions]
	CHECK CONSTRAINT [FK_Sales_CustomerTransactions_InvoiceID_Sales_Invoices]

GO
ALTER TABLE [Sales].[CustomerTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_CustomerTransactions_PaymentMethodID_Application_PaymentMethods]
	FOREIGN KEY ([PaymentMethodID]) REFERENCES [Application].[PaymentMethods] ([PaymentMethodID])
ALTER TABLE [Sales].[CustomerTransactions]
	CHECK CONSTRAINT [FK_Sales_CustomerTransactions_PaymentMethodID_Application_PaymentMethods]

GO
ALTER TABLE [Sales].[CustomerTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_CustomerTransactions_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Sales].[CustomerTransactions]
	CHECK CONSTRAINT [FK_Sales_CustomerTransactions_Application_People]

GO
CREATE CLUSTERED INDEX [CX_Sales_CustomerTransactions]
	ON [Sales].[CustomerTransactions] ([TransactionDate])
	ON [PS_TransactionDate] ([TransactionDate])
GO
CREATE NONCLUSTERED INDEX [FK_Sales_CustomerTransactions_CustomerID]
	ON [Sales].[CustomerTransactions] ([CustomerID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'INDEX', N'FK_Sales_CustomerTransactions_CustomerID'
GO
CREATE NONCLUSTERED INDEX [FK_Sales_CustomerTransactions_TransactionTypeID]
	ON [Sales].[CustomerTransactions] ([TransactionTypeID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'INDEX', N'FK_Sales_CustomerTransactions_TransactionTypeID'
GO
CREATE NONCLUSTERED INDEX [FK_Sales_CustomerTransactions_InvoiceID]
	ON [Sales].[CustomerTransactions] ([InvoiceID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'INDEX', N'FK_Sales_CustomerTransactions_InvoiceID'
GO
CREATE NONCLUSTERED INDEX [FK_Sales_CustomerTransactions_PaymentMethodID]
	ON [Sales].[CustomerTransactions] ([PaymentMethodID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'INDEX', N'FK_Sales_CustomerTransactions_PaymentMethodID'
GO
CREATE NONCLUSTERED INDEX [IX_Sales_CustomerTransactions_IsFinalized]
	ON [Sales].[CustomerTransactions] ([IsFinalized])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Allows quick location of unfinalized transactions', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'INDEX', N'IX_Sales_CustomerTransactions_IsFinalized'
GO
EXEC sp_addextendedproperty N'Description', N'All financial transactions that are customer-related', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used to refer to a customer transaction within the database', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'CustomerTransactionID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer for this transaction', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'CustomerID'
GO
EXEC sp_addextendedproperty N'Description', N'Type of transaction', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'Description', N'ID of an invoice (for transactions associated with an invoice)', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'InvoiceID'
GO
EXEC sp_addextendedproperty N'Description', N'ID of a payment method (for transactions involving payments)', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'PaymentMethodID'
GO
EXEC sp_addextendedproperty N'Description', N'Date for the transaction', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'TransactionDate'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction amount (excluding tax)', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'AmountExcludingTax'
GO
EXEC sp_addextendedproperty N'Description', N'Tax amount calculated', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'TaxAmount'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction amount (including tax)', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'TransactionAmount'
GO
EXEC sp_addextendedproperty N'Description', N'Amount still outstanding for this transaction', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'OutstandingBalance'
GO
EXEC sp_addextendedproperty N'Description', N'Date that this transaction was finalized (if it has been)', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'FinalizationDate'
GO
EXEC sp_addextendedproperty N'Description', N'Is this transaction finalized (invoices, credits and payments have been matched)', 'SCHEMA', N'Sales', 'TABLE', N'CustomerTransactions', 'COLUMN', N'IsFinalized'
GO
ALTER TABLE [Sales].[CustomerTransactions] SET (LOCK_ESCALATION = TABLE)
GO
