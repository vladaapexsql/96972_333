SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Sales].[Orders] (
		[OrderID]                         [int] NOT NULL,
		[CustomerID]                      [int] NOT NULL,
		[SalespersonPersonID]             [int] NOT NULL,
		[PickedByPersonID]                [int] NULL,
		[ContactPersonID]                 [int] NOT NULL,
		[BackorderOrderID]                [int] NULL,
		[OrderDate]                       [date] NOT NULL,
		[ExpectedDeliveryDate]            [date] NOT NULL,
		[CustomerPurchaseOrderNumber]     [nvarchar](20) COLLATE Latin1_General_100_CI_AS NULL,
		[IsUndersupplyBackordered]        [bit] NOT NULL,
		[Comments]                        [nvarchar](max) COLLATE Latin1_General_100_CI_AS NULL,
		[DeliveryInstructions]            [nvarchar](max) COLLATE Latin1_General_100_CI_AS NULL,
		[InternalComments]                [nvarchar](max) COLLATE Latin1_General_100_CI_AS NULL,
		[PickingCompletedWhen]            [datetime2](7) NULL,
		[LastEditedBy]                    [int] NOT NULL,
		[LastEditedWhen]                  [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Sales_Orders]
		PRIMARY KEY
		CLUSTERED
		([OrderID])
	ON [USERDATA]
)
GO
ALTER TABLE [Sales].[Orders]
	ADD
	CONSTRAINT [DF_Sales_Orders_OrderID]
	DEFAULT (NEXT VALUE FOR [Sequences].[OrderID]) FOR [OrderID]
GO
ALTER TABLE [Sales].[Orders]
	ADD
	CONSTRAINT [DF_Sales_Orders_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Sales].[Orders]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_Orders_CustomerID_Sales_Customers]
	FOREIGN KEY ([CustomerID]) REFERENCES [Sales].[Customers] ([CustomerID])
ALTER TABLE [Sales].[Orders]
	CHECK CONSTRAINT [FK_Sales_Orders_CustomerID_Sales_Customers]

GO
ALTER TABLE [Sales].[Orders]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_Orders_SalespersonPersonID_Application_People]
	FOREIGN KEY ([SalespersonPersonID]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Sales].[Orders]
	CHECK CONSTRAINT [FK_Sales_Orders_SalespersonPersonID_Application_People]

GO
ALTER TABLE [Sales].[Orders]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_Orders_PickedByPersonID_Application_People]
	FOREIGN KEY ([PickedByPersonID]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Sales].[Orders]
	CHECK CONSTRAINT [FK_Sales_Orders_PickedByPersonID_Application_People]

GO
ALTER TABLE [Sales].[Orders]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_Orders_ContactPersonID_Application_People]
	FOREIGN KEY ([ContactPersonID]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Sales].[Orders]
	CHECK CONSTRAINT [FK_Sales_Orders_ContactPersonID_Application_People]

GO
ALTER TABLE [Sales].[Orders]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_Orders_BackorderOrderID_Sales_Orders]
	FOREIGN KEY ([BackorderOrderID]) REFERENCES [Sales].[Orders] ([OrderID])
ALTER TABLE [Sales].[Orders]
	CHECK CONSTRAINT [FK_Sales_Orders_BackorderOrderID_Sales_Orders]

GO
ALTER TABLE [Sales].[Orders]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_Orders_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Sales].[Orders]
	CHECK CONSTRAINT [FK_Sales_Orders_Application_People]

GO
CREATE NONCLUSTERED INDEX [FK_Sales_Orders_CustomerID]
	ON [Sales].[Orders] ([CustomerID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'INDEX', N'FK_Sales_Orders_CustomerID'
GO
CREATE NONCLUSTERED INDEX [FK_Sales_Orders_SalespersonPersonID]
	ON [Sales].[Orders] ([SalespersonPersonID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'INDEX', N'FK_Sales_Orders_SalespersonPersonID'
GO
CREATE NONCLUSTERED INDEX [FK_Sales_Orders_PickedByPersonID]
	ON [Sales].[Orders] ([PickedByPersonID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'INDEX', N'FK_Sales_Orders_PickedByPersonID'
GO
CREATE NONCLUSTERED INDEX [FK_Sales_Orders_ContactPersonID]
	ON [Sales].[Orders] ([ContactPersonID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'INDEX', N'FK_Sales_Orders_ContactPersonID'
GO
EXEC sp_addextendedproperty N'Description', N'Detail of customer orders', 'SCHEMA', N'Sales', 'TABLE', N'Orders', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to an order within the database', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'OrderID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer for this order', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'CustomerID'
GO
EXEC sp_addextendedproperty N'Description', N'Salesperson for this order', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'SalespersonPersonID'
GO
EXEC sp_addextendedproperty N'Description', N'Person who picked this shipment', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'PickedByPersonID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer contact for this order', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'ContactPersonID'
GO
EXEC sp_addextendedproperty N'Description', N'If this order is a backorder, this column holds the original order number', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'BackorderOrderID'
GO
EXEC sp_addextendedproperty N'Description', N'Date that this order was raised', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'OrderDate'
GO
EXEC sp_addextendedproperty N'Description', N'Expected delivery date', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'ExpectedDeliveryDate'
GO
EXEC sp_addextendedproperty N'Description', N'Purchase Order Number received from customer', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'CustomerPurchaseOrderNumber'
GO
EXEC sp_addextendedproperty N'Description', N'If items cannot be supplied are they backordered?', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'IsUndersupplyBackordered'
GO
EXEC sp_addextendedproperty N'Description', N'Any comments related to this order (sent to customer)', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'Comments'
GO
EXEC sp_addextendedproperty N'Description', N'Any comments related to order delivery (sent to customer)', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'DeliveryInstructions'
GO
EXEC sp_addextendedproperty N'Description', N'Any internal comments related to this order (not sent to the customer)', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'InternalComments'
GO
EXEC sp_addextendedproperty N'Description', N'When was picking of the entire order completed?', 'SCHEMA', N'Sales', 'TABLE', N'Orders', 'COLUMN', N'PickingCompletedWhen'
GO
ALTER TABLE [Sales].[Orders] SET (LOCK_ESCALATION = TABLE)
GO
