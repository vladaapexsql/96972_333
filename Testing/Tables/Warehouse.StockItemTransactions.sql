SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [Warehouse].[StockItemTransactions] (
		[StockItemTransactionID]      [int] NOT NULL,
		[StockItemID]                 [int] NOT NULL,
		[TransactionTypeID]           [int] NOT NULL,
		[CustomerID]                  [int] NULL,
		[InvoiceID]                   [int] NULL,
		[SupplierID]                  [int] NULL,
		[PurchaseOrderID]             [int] NULL,
		[TransactionOccurredWhen]     [datetime2](7) NOT NULL,
		[Quantity]                    [decimal](18, 3) NOT NULL,
		[LastEditedBy]                [int] NOT NULL,
		[LastEditedWhen]              [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Warehouse_StockItemTransactions]
		PRIMARY KEY
		NONCLUSTERED
		([StockItemTransactionID])
	ON [USERDATA]
)
GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	ADD
	CONSTRAINT [DF_Warehouse_StockItemTransactions_StockItemTransactionID]
	DEFAULT (NEXT VALUE FOR [Sequences].[TransactionID]) FOR [StockItemTransactionID]
GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	ADD
	CONSTRAINT [DF_Warehouse_StockItemTransactions_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemTransactions_StockItemID_Warehouse_StockItems]
	FOREIGN KEY ([StockItemID]) REFERENCES [Warehouse].[StockItems] ([StockItemID])
ALTER TABLE [Warehouse].[StockItemTransactions]
	CHECK CONSTRAINT [FK_Warehouse_StockItemTransactions_StockItemID_Warehouse_StockItems]

GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemTransactions_TransactionTypeID_Application_TransactionTypes]
	FOREIGN KEY ([TransactionTypeID]) REFERENCES [Application].[TransactionTypes] ([TransactionTypeID])
ALTER TABLE [Warehouse].[StockItemTransactions]
	CHECK CONSTRAINT [FK_Warehouse_StockItemTransactions_TransactionTypeID_Application_TransactionTypes]

GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemTransactions_CustomerID_Sales_Customers]
	FOREIGN KEY ([CustomerID]) REFERENCES [Sales].[Customers] ([CustomerID])
ALTER TABLE [Warehouse].[StockItemTransactions]
	CHECK CONSTRAINT [FK_Warehouse_StockItemTransactions_CustomerID_Sales_Customers]

GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemTransactions_InvoiceID_Sales_Invoices]
	FOREIGN KEY ([InvoiceID]) REFERENCES [Sales].[Invoices] ([InvoiceID])
ALTER TABLE [Warehouse].[StockItemTransactions]
	CHECK CONSTRAINT [FK_Warehouse_StockItemTransactions_InvoiceID_Sales_Invoices]

GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemTransactions_SupplierID_Purchasing_Suppliers]
	FOREIGN KEY ([SupplierID]) REFERENCES [Purchasing].[Suppliers] ([SupplierID])
ALTER TABLE [Warehouse].[StockItemTransactions]
	CHECK CONSTRAINT [FK_Warehouse_StockItemTransactions_SupplierID_Purchasing_Suppliers]

GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemTransactions_PurchaseOrderID_Purchasing_PurchaseOrders]
	FOREIGN KEY ([PurchaseOrderID]) REFERENCES [Purchasing].[PurchaseOrders] ([PurchaseOrderID])
ALTER TABLE [Warehouse].[StockItemTransactions]
	CHECK CONSTRAINT [FK_Warehouse_StockItemTransactions_PurchaseOrderID_Purchasing_PurchaseOrders]

GO
ALTER TABLE [Warehouse].[StockItemTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Warehouse_StockItemTransactions_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Warehouse].[StockItemTransactions]
	CHECK CONSTRAINT [FK_Warehouse_StockItemTransactions_Application_People]

GO
CREATE CLUSTERED COLUMNSTORE INDEX [CCX_Warehouse_StockItemTransactions]
	ON [Warehouse].[StockItemTransactions]
	ON [USERDATA]
GO
CREATE NONCLUSTERED INDEX [FK_Warehouse_StockItemTransactions_StockItemID]
	ON [Warehouse].[StockItemTransactions] ([StockItemID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'INDEX', N'FK_Warehouse_StockItemTransactions_StockItemID'
GO
CREATE NONCLUSTERED INDEX [FK_Warehouse_StockItemTransactions_TransactionTypeID]
	ON [Warehouse].[StockItemTransactions] ([TransactionTypeID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'INDEX', N'FK_Warehouse_StockItemTransactions_TransactionTypeID'
GO
CREATE NONCLUSTERED INDEX [FK_Warehouse_StockItemTransactions_CustomerID]
	ON [Warehouse].[StockItemTransactions] ([CustomerID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'INDEX', N'FK_Warehouse_StockItemTransactions_CustomerID'
GO
CREATE NONCLUSTERED INDEX [FK_Warehouse_StockItemTransactions_InvoiceID]
	ON [Warehouse].[StockItemTransactions] ([InvoiceID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'INDEX', N'FK_Warehouse_StockItemTransactions_InvoiceID'
GO
CREATE NONCLUSTERED INDEX [FK_Warehouse_StockItemTransactions_SupplierID]
	ON [Warehouse].[StockItemTransactions] ([SupplierID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'INDEX', N'FK_Warehouse_StockItemTransactions_SupplierID'
GO
CREATE NONCLUSTERED INDEX [FK_Warehouse_StockItemTransactions_PurchaseOrderID]
	ON [Warehouse].[StockItemTransactions] ([PurchaseOrderID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'INDEX', N'FK_Warehouse_StockItemTransactions_PurchaseOrderID'
GO
EXEC sp_addextendedproperty N'Description', N'Transactions covering all movements of all stock items', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used to refer to a stock item transaction within the database', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'StockItemTransactionID'
GO
EXEC sp_addextendedproperty N'Description', N'StockItem for this transaction', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'StockItemID'
GO
EXEC sp_addextendedproperty N'Description', N'Type of transaction', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer for this transaction (if applicable)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'CustomerID'
GO
EXEC sp_addextendedproperty N'Description', N'ID of an invoice (for transactions associated with an invoice)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'InvoiceID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier for this stock transaction (if applicable)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'SupplierID'
GO
EXEC sp_addextendedproperty N'Description', N'ID of an purchase order (for transactions associated with a purchase order)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'PurchaseOrderID'
GO
EXEC sp_addextendedproperty N'Description', N'Date and time when the transaction occurred', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'TransactionOccurredWhen'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity of stock movement (positive is incoming stock, negative is outgoing)', 'SCHEMA', N'Warehouse', 'TABLE', N'StockItemTransactions', 'COLUMN', N'Quantity'
GO
ALTER TABLE [Warehouse].[StockItemTransactions] SET (LOCK_ESCALATION = TABLE)
GO
