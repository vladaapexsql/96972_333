SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Purchasing].[SupplierTransactions] (
		[SupplierTransactionID]     [int] NOT NULL,
		[SupplierID]                [int] NOT NULL,
		[TransactionTypeID]         [int] NOT NULL,
		[PurchaseOrderID]           [int] NULL,
		[PaymentMethodID]           [int] NULL,
		[SupplierInvoiceNumber]     [nvarchar](20) COLLATE Latin1_General_100_CI_AS NULL,
		[TransactionDate]           [date] NOT NULL,
		[AmountExcludingTax]        [decimal](18, 2) NOT NULL,
		[TaxAmount]                 [decimal](18, 2) NOT NULL,
		[TransactionAmount]         [decimal](18, 2) NOT NULL,
		[OutstandingBalance]        [decimal](18, 2) NOT NULL,
		[FinalizationDate]          [date] NULL,
		[IsFinalized]               AS (case when [FinalizationDate] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end) PERSISTED,
		[LastEditedBy]              [int] NOT NULL,
		[LastEditedWhen]            [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Purchasing_SupplierTransactions]
		PRIMARY KEY
		NONCLUSTERED
		([SupplierTransactionID])
	ON [USERDATA]
)
GO
ALTER TABLE [Purchasing].[SupplierTransactions]
	ADD
	CONSTRAINT [DF_Purchasing_SupplierTransactions_SupplierTransactionID]
	DEFAULT (NEXT VALUE FOR [Sequences].[TransactionID]) FOR [SupplierTransactionID]
GO
ALTER TABLE [Purchasing].[SupplierTransactions]
	ADD
	CONSTRAINT [DF_Purchasing_SupplierTransactions_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Purchasing].[SupplierTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_SupplierTransactions_SupplierID_Purchasing_Suppliers]
	FOREIGN KEY ([SupplierID]) REFERENCES [Purchasing].[Suppliers] ([SupplierID])
ALTER TABLE [Purchasing].[SupplierTransactions]
	CHECK CONSTRAINT [FK_Purchasing_SupplierTransactions_SupplierID_Purchasing_Suppliers]

GO
ALTER TABLE [Purchasing].[SupplierTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_SupplierTransactions_TransactionTypeID_Application_TransactionTypes]
	FOREIGN KEY ([TransactionTypeID]) REFERENCES [Application].[TransactionTypes] ([TransactionTypeID])
ALTER TABLE [Purchasing].[SupplierTransactions]
	CHECK CONSTRAINT [FK_Purchasing_SupplierTransactions_TransactionTypeID_Application_TransactionTypes]

GO
ALTER TABLE [Purchasing].[SupplierTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_SupplierTransactions_PurchaseOrderID_Purchasing_PurchaseOrders]
	FOREIGN KEY ([PurchaseOrderID]) REFERENCES [Purchasing].[PurchaseOrders] ([PurchaseOrderID])
ALTER TABLE [Purchasing].[SupplierTransactions]
	CHECK CONSTRAINT [FK_Purchasing_SupplierTransactions_PurchaseOrderID_Purchasing_PurchaseOrders]

GO
ALTER TABLE [Purchasing].[SupplierTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_SupplierTransactions_PaymentMethodID_Application_PaymentMethods]
	FOREIGN KEY ([PaymentMethodID]) REFERENCES [Application].[PaymentMethods] ([PaymentMethodID])
ALTER TABLE [Purchasing].[SupplierTransactions]
	CHECK CONSTRAINT [FK_Purchasing_SupplierTransactions_PaymentMethodID_Application_PaymentMethods]

GO
ALTER TABLE [Purchasing].[SupplierTransactions]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_SupplierTransactions_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Purchasing].[SupplierTransactions]
	CHECK CONSTRAINT [FK_Purchasing_SupplierTransactions_Application_People]

GO
CREATE CLUSTERED INDEX [CX_Purchasing_SupplierTransactions]
	ON [Purchasing].[SupplierTransactions] ([TransactionDate])
	ON [PS_TransactionDate] ([TransactionDate])
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_SupplierTransactions_SupplierID]
	ON [Purchasing].[SupplierTransactions] ([SupplierID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'INDEX', N'FK_Purchasing_SupplierTransactions_SupplierID'
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_SupplierTransactions_TransactionTypeID]
	ON [Purchasing].[SupplierTransactions] ([TransactionTypeID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'INDEX', N'FK_Purchasing_SupplierTransactions_TransactionTypeID'
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_SupplierTransactions_PurchaseOrderID]
	ON [Purchasing].[SupplierTransactions] ([PurchaseOrderID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'INDEX', N'FK_Purchasing_SupplierTransactions_PurchaseOrderID'
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_SupplierTransactions_PaymentMethodID]
	ON [Purchasing].[SupplierTransactions] ([PaymentMethodID])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'INDEX', N'FK_Purchasing_SupplierTransactions_PaymentMethodID'
GO
CREATE NONCLUSTERED INDEX [IX_Purchasing_SupplierTransactions_IsFinalized]
	ON [Purchasing].[SupplierTransactions] ([IsFinalized])
	ON [PS_TransactionDate] ([TransactionDate])
GO
EXEC sp_addextendedproperty N'Description', N'Index used to quickly locate unfinalized transactions', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'INDEX', N'IX_Purchasing_SupplierTransactions_IsFinalized'
GO
EXEC sp_addextendedproperty N'Description', N'All financial transactions that are supplier-related', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used to refer to a supplier transaction within the database', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'SupplierTransactionID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier for this transaction', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'SupplierID'
GO
EXEC sp_addextendedproperty N'Description', N'Type of transaction', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'Description', N'ID of an purchase order (for transactions associated with a purchase order)', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'PurchaseOrderID'
GO
EXEC sp_addextendedproperty N'Description', N'ID of a payment method (for transactions involving payments)', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'PaymentMethodID'
GO
EXEC sp_addextendedproperty N'Description', N'Invoice number for an invoice received from the supplier', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'SupplierInvoiceNumber'
GO
EXEC sp_addextendedproperty N'Description', N'Date for the transaction', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'TransactionDate'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction amount (excluding tax)', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'AmountExcludingTax'
GO
EXEC sp_addextendedproperty N'Description', N'Tax amount calculated', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'TaxAmount'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction amount (including tax)', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'TransactionAmount'
GO
EXEC sp_addextendedproperty N'Description', N'Amount still outstanding for this transaction', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'OutstandingBalance'
GO
EXEC sp_addextendedproperty N'Description', N'Date that this transaction was finalized (if it has been)', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'FinalizationDate'
GO
EXEC sp_addextendedproperty N'Description', N'Is this transaction finalized (invoices, credits and payments have been matched)', 'SCHEMA', N'Purchasing', 'TABLE', N'SupplierTransactions', 'COLUMN', N'IsFinalized'
GO
ALTER TABLE [Purchasing].[SupplierTransactions] SET (LOCK_ESCALATION = TABLE)
GO
