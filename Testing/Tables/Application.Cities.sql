SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Application].[Cities] (
		[CityID]                       [int] NOT NULL,
		[CityName]                     [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[StateProvinceID]              [int] NOT NULL,
		[Location]                     [geography] NULL,
		[LatestRecordedPopulation]     [bigint] NULL,
		[LastEditedBy]                 [int] NOT NULL,
		[ValidFrom]                    [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]                      [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [PK_Application_Cities]
		PRIMARY KEY
		CLUSTERED
		([CityID])
	ON [USERDATA]
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Application].[Cities_Archive]))
GO
ALTER TABLE [Application].[Cities]
	ADD
	CONSTRAINT [DF_Application_Cities_CityID]
	DEFAULT (NEXT VALUE FOR [Sequences].[CityID]) FOR [CityID]
GO
ALTER TABLE [Application].[Cities]
	WITH CHECK
	ADD CONSTRAINT [FK_Application_Cities_StateProvinceID_Application_StateProvinces]
	FOREIGN KEY ([StateProvinceID]) REFERENCES [Application].[StateProvinces] ([StateProvinceID])
ALTER TABLE [Application].[Cities]
	CHECK CONSTRAINT [FK_Application_Cities_StateProvinceID_Application_StateProvinces]

GO
ALTER TABLE [Application].[Cities]
	WITH CHECK
	ADD CONSTRAINT [FK_Application_Cities_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Application].[Cities]
	CHECK CONSTRAINT [FK_Application_Cities_Application_People]

GO
CREATE NONCLUSTERED INDEX [FK_Application_Cities_StateProvinceID]
	ON [Application].[Cities] ([StateProvinceID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Application', 'TABLE', N'Cities', 'INDEX', N'FK_Application_Cities_StateProvinceID'
GO
EXEC sp_addextendedproperty N'Description', N'Cities that are part of any address (including geographic location)', 'SCHEMA', N'Application', 'TABLE', N'Cities', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a city within the database', 'SCHEMA', N'Application', 'TABLE', N'Cities', 'COLUMN', N'CityID'
GO
EXEC sp_addextendedproperty N'Description', N'Formal name of the city', 'SCHEMA', N'Application', 'TABLE', N'Cities', 'COLUMN', N'CityName'
GO
EXEC sp_addextendedproperty N'Description', N'State or province for this city', 'SCHEMA', N'Application', 'TABLE', N'Cities', 'COLUMN', N'StateProvinceID'
GO
EXEC sp_addextendedproperty N'Description', N'Geographic location of the city', 'SCHEMA', N'Application', 'TABLE', N'Cities', 'COLUMN', N'Location'
GO
EXEC sp_addextendedproperty N'Description', N'Latest available population for the City', 'SCHEMA', N'Application', 'TABLE', N'Cities', 'COLUMN', N'LatestRecordedPopulation'
GO
ALTER TABLE [Application].[Cities] SET (LOCK_ESCALATION = TABLE)
GO
