SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Purchasing].[PurchaseOrderLines] (
		[PurchaseOrderLineID]           [int] NOT NULL,
		[PurchaseOrderID]               [int] NOT NULL,
		[StockItemID]                   [int] NOT NULL,
		[OrderedOuters]                 [int] NOT NULL,
		[Description]                   [nvarchar](100) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[ReceivedOuters]                [int] NOT NULL,
		[PackageTypeID]                 [int] NOT NULL,
		[ExpectedUnitPricePerOuter]     [decimal](18, 2) NULL,
		[LastReceiptDate]               [date] NULL,
		[IsOrderLineFinalized]          [bit] NOT NULL,
		[LastEditedBy]                  [int] NOT NULL,
		[LastEditedWhen]                [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Purchasing_PurchaseOrderLines]
		PRIMARY KEY
		CLUSTERED
		([PurchaseOrderLineID])
	ON [USERDATA]
)
GO
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	ADD
	CONSTRAINT [DF_Purchasing_PurchaseOrderLines_PurchaseOrderLineID]
	DEFAULT (NEXT VALUE FOR [Sequences].[PurchaseOrderLineID]) FOR [PurchaseOrderLineID]
GO
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	ADD
	CONSTRAINT [DF_Purchasing_PurchaseOrderLines_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrderLines_PurchaseOrderID_Purchasing_PurchaseOrders]
	FOREIGN KEY ([PurchaseOrderID]) REFERENCES [Purchasing].[PurchaseOrders] ([PurchaseOrderID])
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrderLines_PurchaseOrderID_Purchasing_PurchaseOrders]

GO
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrderLines_StockItemID_Warehouse_StockItems]
	FOREIGN KEY ([StockItemID]) REFERENCES [Warehouse].[StockItems] ([StockItemID])
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrderLines_StockItemID_Warehouse_StockItems]

GO
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrderLines_PackageTypeID_Warehouse_PackageTypes]
	FOREIGN KEY ([PackageTypeID]) REFERENCES [Warehouse].[PackageTypes] ([PackageTypeID])
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrderLines_PackageTypeID_Warehouse_PackageTypes]

GO
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrderLines_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Purchasing].[PurchaseOrderLines]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrderLines_Application_People]

GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_PurchaseOrderLines_PurchaseOrderID]
	ON [Purchasing].[PurchaseOrderLines] ([PurchaseOrderID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'INDEX', N'FK_Purchasing_PurchaseOrderLines_PurchaseOrderID'
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_PurchaseOrderLines_StockItemID]
	ON [Purchasing].[PurchaseOrderLines] ([StockItemID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'INDEX', N'FK_Purchasing_PurchaseOrderLines_StockItemID'
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_PurchaseOrderLines_PackageTypeID]
	ON [Purchasing].[PurchaseOrderLines] ([PackageTypeID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'INDEX', N'FK_Purchasing_PurchaseOrderLines_PackageTypeID'
GO
CREATE NONCLUSTERED INDEX [IX_Purchasing_PurchaseOrderLines_Perf_20160301_4]
	ON [Purchasing].[PurchaseOrderLines] ([IsOrderLineFinalized], [StockItemID])
	INCLUDE ([OrderedOuters], [ReceivedOuters])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Improves performance of order picking and invoicing', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'INDEX', N'IX_Purchasing_PurchaseOrderLines_Perf_20160301_4'
GO
EXEC sp_addextendedproperty N'Description', N'Detail lines from supplier purchase orders', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a line on a purchase order within the database', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'PurchaseOrderLineID'
GO
EXEC sp_addextendedproperty N'Description', N'Purchase order that this line is associated with', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'PurchaseOrderID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item for this purchase order line', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'StockItemID'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity of the stock item that is ordered', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'OrderedOuters'
GO
EXEC sp_addextendedproperty N'Description', N'Description of the item to be supplied (Often the stock item name but could be supplier description)', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'Description', N'Total quantity of the stock item that has been received so far', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'ReceivedOuters'
GO
EXEC sp_addextendedproperty N'Description', N'Type of package received', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'PackageTypeID'
GO
EXEC sp_addextendedproperty N'Description', N'The unit price that we expect to be charged', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'ExpectedUnitPricePerOuter'
GO
EXEC sp_addextendedproperty N'Description', N'The last date on which this stock item was received for this purchase order', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'LastReceiptDate'
GO
EXEC sp_addextendedproperty N'Description', N'Is this purchase order line now considered finalized? (Receipted quantities and weights are often not precise)', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrderLines', 'COLUMN', N'IsOrderLineFinalized'
GO
ALTER TABLE [Purchasing].[PurchaseOrderLines] SET (LOCK_ESCALATION = TABLE)
GO
